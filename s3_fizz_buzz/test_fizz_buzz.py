import pytest
from collections import OrderedDict

from .fizz_buzz import get_fizz_buzz


class Case:
    def __init__(self, n, expected):
        self.n = n
        self.expected = expected


TEST_CASES = OrderedDict([
    ("test_case_1", Case(n=1, expected={0: 1})),
    ("test_case_2", Case(n=2, expected={0: 1, 1: 2})),
    ("test_case_3", Case(n=3, expected={0: 1, 1: 2, 2: "Fizz"})),
    ("check_fizz_buzz", Case(n=100, expected={i - 1 : "Fizz Buzz" for i in range(15, 101, 15)})),
    ("check_fizz", Case(n=100, expected={i - 1: "Fizz" for i in range(3, 101, 3) if i % 15 != 0})),
    ("check_buzz", Case(n=100, expected={i - 1: "Buzz" for i in range(5, 101, 5) if i % 15 != 0})),
    ("check_digits", Case(n=100, expected={i - 1: i for i in range(5, 101, 5) if i % 3 != 0 and i % 5 != 0})),

])


@pytest.mark.parametrize("test_case", TEST_CASES.values(), ids=list(TEST_CASES.keys()))
def test_get_fizz_buzz(test_case):
    fizz_buzz_list = get_fizz_buzz(test_case.n)
    assert len(fizz_buzz_list) == test_case.n
    for key, value in test_case.expected.items():
        assert fizz_buzz_list[key] == value
