import pandas as pd


def find_most_severe_pump_and_dump(
    coins: pd.DataFrame, symbol: str, start_date: str, end_date: str
) -> dict:
    """
    :param coins: coins open, close, high prices (in USD)
    and names for each date
    :param symbol: name of the traded coin
    :param start_date: begining of the date range
    :param end_date: end of the date range
    :return: date and value of maximal pump and dump
    """
    pass
