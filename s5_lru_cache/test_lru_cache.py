from datetime import datetime
import sys

from .lru_cache import cache

sys.setrecursionlimit(100000)


def test_etalon():
    usage_counter = 0

    @cache(2)
    def foo(value):
        nonlocal usage_counter
        usage_counter += 1
        return value
    
    assert usage_counter == 0

    foo(1)
    foo(2)
    foo(1)
    assert usage_counter == 2
    foo(2)
    assert usage_counter == 2
    foo(3)
    assert usage_counter == 3
    foo(2)
    assert usage_counter == 3
    foo(1)
    assert usage_counter == 4


def test_use_ft():
    @cache(1)
    def foo():
        return 1

    assert not (hasattr(foo, 'cache_clear') or hasattr(foo, 'cache_info')), 'Hey! Do not use lru_cache from functools'


def test_akkerman():
    @cache(2048)
    def akkerman(m, n):
        """I am Akkerman and it is my doc"""
        if m == 0:
            return n + 1
        if m > 0 and n == 0:
            return akkerman(m - 1, 1)
        if m > 0 and n > 0:
            return akkerman(m - 1, akkerman(m, n - 1))

    expected_result = 16381
    start = datetime.now()
    result = akkerman(3, 11)
    time_taken = (datetime.now() - start).total_seconds()

    assert result == expected_result, 'Your decorator makes the Akkerman function return wrong value'
    assert time_taken < 2, 'Your decorator does not make the Akkerman function work faster'

    expected_doc = 'I am Akkerman and it is my doc'
    assert akkerman.__doc__ == expected_doc, 'Your decorator deletes docs'


def test_join_lists():
    @cache(1)
    def join(args):
        result = ()
        for l in args:
            k = join(l) if isinstance(l, tuple) else (l,)
            result += k
        return result

    result = join(((1, 2, 3), 1, (1, 2, 3, 4, ((1, 2), 2, 3))))
    assert result == (1, 2, 3, 1, 1, 2, 3, 4, 1, 2, 2, 3)


def test_not_cached():
    @cache(1)
    def not_cached_akkerman(m, n):
        if m == 0:
            return n + 1
        if m > 0 and n == 0:
            return not_cached_akkerman(m - 1, 1)
        if m > 0 and n > 0:
            return not_cached_akkerman(m - 1, not_cached_akkerman(m, n - 1))

    start = datetime.now()
    _ = not_cached_akkerman(3, 8)
    time_taken = (datetime.now() - start).total_seconds()
    assert time_taken > 1
