from .etl import transform


def test_etalon():
    legacy_data = {
        1: ['foo', 'BAR'],
        2: ['spam'],
        3: ['Foo'],
    }
    data = {
        'foo': [1, 3],
        'bar': [1],
        'spam': [2],
    }
    assert transform(legacy_data) == data


def test_etalon_two():
    legacy_data = {
        1: ['foo', 'foo', 'Foo'],
    }
    data = {
        'foo': [1],
    }
    assert transform(legacy_data) == data


def test_empty_input():
    assert transform({}) == {}


def test_one_key_one_value():
    assert transform({1: ['A']}) == {'a': [1]}


def test_one_key_multiple_values():
    legacy_data = {1: ['A', 'e', 'I', 'O', 'u']}
    data = {'a': [1], 'e': [1], 'i': [1], 'o': [1], 'u': [1]}
    assert transform(legacy_data) == data


def test_multiple_values_with_duplicates():
    legacy_data = {1: ['A', 'e', 'E', 'I', 'O', 'u', 'e'], -3: ['e', 'E']}
    data = {'a': [1], 'e': [1, -3], 'i': [1], 'o': [1], 'u': [1]}
    assert transform(legacy_data) == data


def test_multiple_keys_multiple_values():
    legacy_data = {1: ['A', 'E'], 2: ['d', 'G']}
    data = {'a': [1], 'd': [2], 'e': [1], 'g': [2]}
    assert transform(legacy_data) == data
