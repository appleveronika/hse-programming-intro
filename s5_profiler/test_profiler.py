from collections import namedtuple
import time
import sys

from .profiler import profiler

sys.setrecursionlimit(100000)


def test_simple():
    @profiler
    def rec(n):
        if n > 0:
            return rec(n - 1)
        else:
            return 1

    rec(3)
    assert rec.calls == 4


def test_last_time_taken():
    @profiler
    def foo():
        time.sleep(1)
        return 'bar'

    foo()
    foo()
    foo()
    assert foo.calls == 1
    assert foo.last_time_taken >= 1


def test_akkerman():
    @profiler
    def akkerman(m, n):
        """I am Akkerman and it is my doc"""
        if m == 0:
            return n + 1
        if m > 0 and n == 0:
            return akkerman(m - 1, 1)
        if m > 0 and n > 0:
            return akkerman(m - 1, akkerman(m, n - 1))

    expected_calls = 541
    result = akkerman(3, 2)
    assert akkerman.calls == expected_calls
    assert result == 29

    expected_calls = 541
    _ = akkerman(0, 1)
    _ = akkerman(3, 2)
    assert akkerman.calls == expected_calls

    _ = akkerman(3, 8)

    expected_doc = 'I am Akkerman and it is my doc'
    assert akkerman.__doc__ == expected_doc, 'Your decorator deletes docs'


def test_strange_function():
    @profiler
    def strange_function(a, b, c=1, d=2):
        return a.foo + b.bar + c + d

    nt = namedtuple('foo', ['foo', 'bar'])
    expected_result = 10
    result = strange_function(nt(1, 2), nt(1, 2), 3, 4)
    assert result == expected_result, 'Your decorator makes the function work wrong'
